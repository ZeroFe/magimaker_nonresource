﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class #SCRIPTNAME# : ScriptableObject, ISerializationCallbackReceiver
{
    public void OnAfterDeserialize()
    {
        Debug.Log("On After Deserialize");
    }

    public void OnBeforeSerialize()
    {
        Debug.Log("On Before Deserialize");
    }
}
